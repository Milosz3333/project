from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
from uuid import UUID
import models as models
from database import engine, SessionLocal
from sqlalchemy.orm import Session

app = FastAPI()

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class Book(BaseModel):
    id: int
    title: str
    author: str
    price: int


BOOKS = []


@app.get("/", tags=["books"])
def get_book(db: Session = Depends(get_db)):
    return db.query(models.Books).all()


@app.post("/", tags=["books"])
def add_book(book: Book, db: Session = Depends(get_db)):

    book_model = models.Books()
    book_model.title = book.title
    book_model.author = book.author
    book_model.price = book.price

    db.add(book_model)
    db.commit()

    return book


@app.put("/{book_id}", tags=["books"])
def update_book(book_id: int, book: Book, db: Session = Depends(get_db)):

    book_model = db.query(models.Books).filter(models.Books.id == book.id).first()

    if book_model is None:
        raise HTTPException(status_code=404, detail=f"ID{book_id}: Does not exist")

    book_model.title = book.title
    book_model.author = book.author
    book_model.price = book.price

    db.add(book_model)
    db.commit

    return book


@app.delete("/{book_id}", tags=["books"])
def delete_book(book_id: int, db: Session = Depends(get_db)):

    book_model = db.query(models.Books).filter(models.Books.id == book_id).first()

    if book_model is None:
        raise HTTPException(status_code=404, detail=f"ID{book_id}: Does not exist")

    db.query(models.Books).filter(models.Books.id == book_id).delete()
